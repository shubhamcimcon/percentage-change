import pandas as pd
import matplotlib.pyplot as plt
import json
import time
from requests.structures import CaseInsensitiveDict
import requests

def toInt(x):
    if x.strip().isdigit():
        return int(x)
    return 0

def getfloat(x):
    return float(x,9)

def str_to_list(data : str) -> list:
    data = data[1:-1]
    data = data.split(",")
    return list(map(float,data))

def getTime(x):
    return time.ctime(x/1000)

def get_data(device_id:  str, parameters, interval, aggregator, startTs, endTs):
    token=get_thingsboard_token(config["thingsboard_ip"],config["tanent_username"],config["tanent_password"])
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    records = 50000
    thingsboard_ip = config["thingsboard_ip"]
    # parameters = get_parameter_string(parameters)
    url = f"http://{thingsboard_ip}/api/plugins/telemetry/DEVICE/{device_id}/values/timeseries?interval={interval}&limit={records}&agg={aggregator}&orderBy=DESC&useStrictDataTypes=false&keys={parameters}&startTs={startTs}&endTs={endTs}"
    data = requests.get(url, headers=headers)
    if data.status_code == 200:
        data = data.json()
    else:
        data=""
    return (data)

def get_thingsboard_token(thingsboard_ip, username, password):
    headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    }
    data = '{"username":"'+username+'", "password":"'+password+'"}'
    response = requests.post('http://'+thingsboard_ip+'/api/auth/login', headers=headers, data=data)
    token = response.json()['token']
    return token


with open("config.json") as file:
    config=json.load(file)
freq = 120
fbin = int(freq * config['delF'])

rpm_low = config['RPM'] - 60*config['delF']
rpm_high =config['RPM'] + 60*config['delF'] 
device_id="fa62dfc0-567c-11ec-bc70-f3dc403e1829"
startTs = config['startTs']
endTs =round(time.time()*1000)
data_store=get_data(device_id, config["telemetry_parameters"], config["interval"],config["aggregator"],config["startTs"], endTs)

fft = list(map(str_to_list,list(pd.json_normalize(data_store['X-Axis Acceleration FFT'])['value'])))
ts = list(map(int,list(pd.json_normalize(data_store['X-Axis Acceleration FFT'])['ts'])))
cal_rpm =list(map(toInt,list(pd.json_normalize(data_store['cal_rpm'])['value'])))
trendpoints = []
ts_for_trend = []

for idx, val in enumerate(cal_rpm):
    if rpm_low < val < rpm_high:
        trendpoints.append(fft[idx][fbin])
        ts_for_trend.append(ts[idx])
trend = pd.DataFrame(trendpoints, columns=["datapoints"]).rolling(window=int(len(trendpoints)*0.1)).mean()
ts_for_trend = list(map(getTime, ts_for_trend))
plt.plot(ts_for_trend,list(trend['datapoints']))
plt.scatter(ts_for_trend,trendpoints)
plt.xticks(rotation = 90)
plt.savefig("test.png")
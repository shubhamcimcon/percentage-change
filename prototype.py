import pandas as pd
import matplotlib.pyplot as plt
import json
import time
from requests.structures import CaseInsensitiveDict
import requests
import numpy as np
from ipyvizzu import Chart, Data, Config
data = Data()

def toInt(x):
    if x.strip().isdigit():
        return int(x)
    return 0

def getfloat(x):
    return float(x,9)

def str_to_list(data : str) -> list:
    data = data[1:-1]
    data = data.split(",")
    return list(map(float,data))

def getTime(x):
    return time.ctime(x/1000)

def get_data(device_id:  str, parameters, interval, aggregator, startTs, endTs):
    token=get_thingsboard_token(config["thingsboard_ip"],config["tanent_username"],config["tanent_password"])
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    records = 50000
    thingsboard_ip = config["thingsboard_ip"]
    # parameters = get_parameter_string(parameters)
    url = f"http://{thingsboard_ip}/api/plugins/telemetry/DEVICE/{device_id}/values/timeseries?interval={interval}&limit={records}&agg={aggregator}&orderBy=DESC&useStrictDataTypes=false&keys={parameters}&startTs={startTs}&endTs={endTs}"
    data = requests.get(url, headers=headers)
    if data.status_code == 200:
        data = data.json()
    else:
        data=""
    return (data)

def get_thingsboard_token(thingsboard_ip, username, password):
    headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    }
    data = '{"username":"'+username+'", "password":"'+password+'"}'
    response = requests.post('http://'+thingsboard_ip+'/api/auth/login', headers=headers, data=data)
    token = response.json()['token']
    return token


with open("config.json") as file:
    config=json.load(file)
freq = 5.6962
fbin = int(round(freq / config['delF']))

rpm_low = config['RPM'] - 60*config['delF']
rpm_high =config['RPM'] + 60*config['delF'] 
device_id="f6564701-a050-11ec-990f-db0a2124af7e"
startTs = config['startTs']
endTs =round(time.time()*1000)
# endTs = 1648596600000
data_store=get_data(device_id, config["telemetry_parameters"], config["interval"],config["aggregator"],config["startTs"], endTs)


fft = list(map(str_to_list,list(pd.json_normalize(data_store['X-Axis Acceleration FFT'])['value'])))
ts = list(map(int,list(pd.json_normalize(data_store['X-Axis Acceleration FFT'])['ts'])))
cal_rpm =list(map(toInt,list(pd.json_normalize(data_store['cal_rpm'])['value'])))

df=pd.DataFrame({"ts":ts, "fft":fft, "cal_rpm":cal_rpm})
df.sort_values("ts",inplace=True)
trendpoints = []
ts_for_trend = []

for idx, val in enumerate(df.cal_rpm.to_list()):
    if rpm_low < val < rpm_high:
        trendpoints.append(df.fft.to_list()[idx][fbin])
        ts_for_trend.append(df.ts.to_list()[idx])
# window=int(len(trendpoints)*0.3333333333333333)
# ts_for_trend = list(map(getTime, ts_for_trend))
# trend = pd.DataFrame(data=[ts_for_trend,trendpoints]).transpose()
# trend.columns = ["ts_for_trend","datapoints"]
# trend_df=trend.rolling(window=window).mean()
# trend_df_nan=trend_df.iloc[window-1:]

window=int(len(trendpoints)*0.1)
window=6
ts_for_trend = list(map(getTime, ts_for_trend))
trend = pd.DataFrame(trendpoints, columns=["datapoints"]).rolling(window=window).mean()
trend.columns = ["datapoints"]
trend_df_nan=trend.iloc[window-1:]

ts_for_trend_df = ts_for_trend[window-1:]
trendpoints_df = trend_df_nan["datapoints"].to_list()

data.add_series("ts_for_trend_df", ts_for_trend_df)
data.add_series("trendpoints_df", trendpoints_df)
data.add_series("ts_for_trend", ts_for_trend)
data.add_series("trendpoints", trendpoints)
chart = Chart()
chart.animate(data)

chart.animate(Config({"x": "ts_for_trend", "y": "trendpoints", "color": "trendpoints"}))
chart.animate(Config({"geometry": "line"}))

chart.animate(Config({"x": "ts_for_trend_df", "y": "trendpoints_df", "color": "ts_for_trend_df"}))
chart.animate(Config({"geometry": "line"}))


# plt.figure(figsize=(len(trendpoints)*0.75,5))
# plt.scatter(ts_for_trend,trendpoints)
# plt.plot(ts_for_trend_df,trendpoints_df)
# plt.xticks(np.array(ts_for_trend),rotation = 90)
# plt.yticks(np.arange(min(trendpoints),max(trendpoints),0.00029821917))
# plt.grid()
# plt.savefig("test.png")

# plt.scatter(list(trend['ts_for_trend']),list(trend['datapoints']))
# plt.plot(list(trend_df_nan['ts_for_trend']),list(trend_df_nan['datapoints']))
# plt.xticks(np.array(ts_for_trend),rotation = 90)
# plt.yticks(np.arange(min(trendpoints),max(trendpoints),0.00069821917),rotation = 90)
# plt.savefig("test.png")